"use strict";

var app = require("app"),
  BrowserWindow = require("browser-window"),
  Tray = require("tray"), Menu = require("menu");

require("crash-reporter").start();

var mainWindow = null;

app.on("window-all-closed", function() {
  if (process.platform !== "darwin"){
    app.quit();
  }
});

app.on("ready", function() {
  mainWindow = new BrowserWindow({
    show:false
//    width: 1,
//    height: 1,
//    transparent: true,
//    frame: false
  });
  mainWindow.loadUrl("file://" + __dirname + '/index.html');

  var app_icon = new Tray(__dirname + "/img/tt-icon.png"),
    context_menu = Menu.buildFromTemplate([
      {label: "Quit", accelerator: 'Command+Q', click: function() { app.quit(); }}
    ]);
  app_icon.setContextMenu(context_menu);
  mainWindow.on("closed", function() {
    mainWindow = null;
  });
});
