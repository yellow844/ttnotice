"use strict";
var config = require("./config"),
  remote = require("remote"),
  shell = remote.require("shell"),
  env = require('node-env-file'),
  Q = require('q'),
  Twitter = require("twitter"),
  ttscraper = require("ttscraper"),
  _twitter, intervalID;

env(__dirname + '/.env');

_twitter = new Twitter({
  consumer_key: process.env.consumer_key,
  consumer_secret: process.env.consumer_secret,
  access_token_key: process.env.access_token_key,
  access_token_secret: process.env.access_token_secret
});

function notice(title, body, path, icon) {
  var _notice = new Notification(title, {
    icon: icon,
    body: body
  });
  _notice.onclick = function () {
    shell.openExternal(path);
  };
}

function tmp_notice(id, key, title, body, path, icon) {
  var tmp_id = localStorage.getItem(key);
  if (tmp_id === id) return false;
  notice(title, body, path, icon);
  localStorage.setItem(key, id);
}

function _promise_twitter(screen_name) {
  var key_prefix = "twitter_";
  return new Promise(function (resolve) {
    _twitter.get('statuses/user_timeline.json', { screen_name: screen_name, count: 1}, function (error, tweets, response) {
      if (error === null) {
        let latest = tweets[0],
          key = key_prefix + screen_name,
          id_str = latest.id_str;
        tmp_notice(id_str, key, "Tweeted!! " + latest.user.name, latest.text, "https://twitter.com/" + screen_name + "/status/" + id_str, latest.user.profile_image_url);
        resolve(tweets);
      }
    });
  });
}

function _twitter_process(){
  Q.all(config.twitter.screen_names.forEach(function (screen_name) {
    _promise_twitter(screen_name);
  }));
}

function _blog_process(){
  var key_prefix = "blog_", _blog = ttscraper.blog, config_blog = config.blog;
  Object.keys(_blog).forEach(function (key) {
    _blog[key].then(function(data){
      var latest = data.items[0];
      tmp_notice("" + latest.pubDate, key_prefix + key, "Blog " + data.meta.title, latest.title, latest.link, config_blog[key].img);
    });
  });
}

function _official_process(){
  var key = "official", _official = ttscraper.official.whatsNew;
  ttscraper.official.whatsNew.then(function(data){
    var latest = data[0];
    tmp_notice("" + latest.date + latest.text, key, "Official What's New!!", latest.text, latest.path, config.official.img);
  });
}

function _process() {
  _twitter_process();
  _blog_process();
  _official_process();
}


localStorage.clear();

_process();

intervalID = setInterval(_process, 60 * 1000 * 10);
remote.getCurrentWindow().on("close", function() {
  clearInterval(intervalID);
});