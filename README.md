TTNotice v0.1.0

このApplicationは神田沙也加さんとBillyさんの音楽ユニット[『TRUSTRICK』](http://columbia.jp/trustrick/index.html)の最新情報を通知するためのアプリです
起動時と10分毎にTwitter、Blog、公式What's Newをチェックしています
最新の情報があれば通知機能でお知らせしています

[DL](https://bitbucket.org/yellow844/ttnotice/downloads)

9月14日現在、Mac(TTNotice-darwin-x64)用のApplicationを試験運用中です  
Winは現在検証中です

今後の予定としては、ポーリング時間の設定と、
取得情報の一覧機能を予定しています

現在試験中ですので、ご使用に関しましては自己責任でお願い致します

不具合などに関しましては [issue](https://bitbucket.org/yellow844/ttnotice/issues) もしくは、
[@yellow844](https://twitter.com/yellow844) までご連絡下さい  
また、TRUSTRICK関連での欲しいApplicationなどのリクエストが有ればお待ちしております。

本アプリは [Electron](http://electron.atom.io/) にて制作されています